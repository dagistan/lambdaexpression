package dagistan.sample.lambda;

interface Lambda {

	int SumNumbers(int a);

}

public class LambdaExample {

	public static void main(String[] args) {
		Lambda l;
		l = (a) -> a + 1010;

		int result = 0;
		result = l.SumNumbers(25);

		System.out.println(result);
	}

}
